/*
    MainActivity.java
    Project for EngagingTech's Android Coding Test
    Created on: 1/2/19
    Created by Joe Chung
 */

package com.jc.engagingtech;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private LinearLayout progressBarLinearLayout;
    private Button button;
    private final static String userUrl = "https://private-bc5bb-engagingtech.apiary-mock.com/user";
    private boolean resume = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBarLinearLayout = findViewById(R.id.main_progress_bar_linear_layout);
        button = findViewById(R.id.button_load_data);
    }

    public void loadUserData(View view) {
        new MyAsyncTask().execute(userUrl);
    }

    private class MyAsyncTask extends AsyncTask<String, String, String> {

        private StringBuilder sb = new StringBuilder();

        protected void onPreExecute() {
            super.onPreExecute();
            // Set the layout with the progress bar and text to visible
            progressBarLinearLayout.setVisibility(View.VISIBLE);
            button.setVisibility(View.INVISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            InputStream is;
            URL url;
            try {
                // Read the data from the URL
                url = new URL(userUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                is = connection.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                connection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Post the results using intent to next activity
            Intent intent = new Intent(getApplicationContext(), ShowUserActivity.class);
            intent.putExtra(Global.USER_DATA, sb.toString());
            startActivity(intent);
        }

    }

    // onResume feature if the user presses the 'back' button to remove the progress bar layout
    @Override
    protected void onResume() {
        super.onResume();
        if (resume) {
            button.setVisibility(View.VISIBLE);
            progressBarLinearLayout.setVisibility(View.INVISIBLE);
        } else {
            resume = true;
        }
    }

}
