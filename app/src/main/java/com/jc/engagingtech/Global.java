/*
    Global.java
    Constants file to store any 'Global' variables
    Created: 2/2/19
    Created by Joe Chung
 */

package com.jc.engagingtech;

class Global {

    static final String USER_DATA = "userData";
    static final String USER_NAME = "name";
    static final String USER_PROFILE_URL = "profile_url";
    static final String USER_NUMBER = "user_number";
    static final String USER_TELEPHONES = "telephone_numbers";
    static final String USER_PHONE_TYPE = "type";
    static final String USER_PHONE_NUMBER = "number";

}
