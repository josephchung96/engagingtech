/*
    ShowUserActivity.java
    Activity to display the user's information
    Created: 1/2/19
    Created by Joe Chung
 */

package com.jc.engagingtech;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;

public class ShowUserActivity extends AppCompatActivity {

    private TextView tvName, tvUserNumber;
    private ImageView profileImage;
    private LinearLayout linearLayout;
    private String profileUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user);

        String json = getIntent().getStringExtra(Global.USER_DATA);

        linearLayout = findViewById(R.id.view_user_ll);
        profileImage = findViewById(R.id.view_user_image);
        tvName = findViewById(R.id.view_user_name);
        tvUserNumber = findViewById(R.id.view_user_number);

        decodeJson(json);
    }

    private void decodeJson(String json) {
        // Break down the json string using JSON library
        try {
            JSONObject jsonObject = new JSONObject(json);
            String name = validateName(jsonObject.getString(Global.USER_NAME).trim());
            profileUrl = jsonObject.getString(Global.USER_PROFILE_URL);
            int userNumber = jsonObject.getInt(Global.USER_NUMBER);
            JSONArray telephoneNumbers = jsonObject.getJSONArray(Global.USER_TELEPHONES);
            new MyAsyncTask().execute(profileUrl);
            // Dynamically add telephone numbers
            for (int i = 0; i< telephoneNumbers.length(); i++) {
                JSONObject obj = telephoneNumbers.getJSONObject(i);
                addTextView(obj);
            }
            String userNumberString = "#" + String.valueOf(userNumber);
            tvUserNumber.setText(String.valueOf(userNumberString));
            tvName.setText(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addTextView(JSONObject obj) {
        try {
            String type = obj.getString(Global.USER_PHONE_TYPE);
            String phoneNumber = validatePhone(obj.getString(Global.USER_PHONE_NUMBER).trim());
            String phoneString = type + ": " + phoneNumber;
            // Formatting layout of the TextView's
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 25);
            TextView tv = new TextView(this);
            tv.setTextAppearance(this, android.R.style.TextAppearance);
            tv.setLayoutParams(params);
            tv.setText(phoneString);
            linearLayout.addView(tv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String validatePhone(String phoneNumber) {
        phoneNumber = phoneNumber.replaceAll("\n","").replaceAll("\r", "").replaceAll(" +", "");
        // Use StringBuilder to format the phone number
        StringBuilder sb = new StringBuilder(phoneNumber)
                .insert(3, " ")
                .insert(4, "(")
                .insert(8, ")")
                .insert(9, " ")
                .insert(13, "-");
        return sb.toString();
    }

    // Removes all spaces and return carriages and extra spaces
    private String validateName(String name) {
        return name.replaceAll("\n", "").replaceAll("\r", "").replaceAll(" +", " ");
    }

    // Loads the image into the ImageView in rounded border format
    private class MyAsyncTask extends AsyncTask<String, Void, RoundedBitmapDrawable> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected RoundedBitmapDrawable doInBackground(String... strings) {
            InputStream is;
            RoundedBitmapDrawable roundedBitmapDrawable = null;
            float cornerRadius = 70.0f;
            try {
                is = new URL(profileUrl).openStream();
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                roundedBitmapDrawable.setCornerRadius(cornerRadius);
                roundedBitmapDrawable.setAntiAlias(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return roundedBitmapDrawable;
        }

        @Override
        protected void onPostExecute(RoundedBitmapDrawable draw) {
            profileImage.setImageDrawable(draw);
        }
    }

}
